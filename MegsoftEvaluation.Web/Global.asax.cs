﻿using System;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Elmah.Mvc;
using MegsoftEvaluation.Web.Controllers;
using MegsoftEvaluation.Web.Models;

namespace MegsoftEvaluation.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        //http://stackoverflow.com/a/9026907/2187991
        protected void Application_EndRequest()
        {
            var customErrorHandle = Context.Response.StatusCode.ToString().StartsWith("4") || Context.Response.StatusCode.ToString().StartsWith("5");
            if (!customErrorHandle) return;
            
            var routeData = new RouteData();
            routeData.Values["controller"] = "Errors";
            routeData.Values["action"] = Context.Response.StatusCode.ToString().StartsWith("4") ? "NotFound" : "Index"; 

            Context.Response.Clear();
            Context.Server.ClearError();

            // Avoid IIS7 getting in the middle
            Response.TrySkipIisCustomErrors = true;
            IController errormanagerController = new ErrorsController();
            var wrapper = new HttpContextWrapper(Context);
            var rc = new RequestContext(wrapper, routeData);
            errormanagerController.Execute(rc);
        }
    }
}