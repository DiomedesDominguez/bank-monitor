﻿/// <reference path="../_references.js" />


var SettingsInit = function () {

    $.get(window.AppViewModel.Banks, function (data) {
        var existingBanks = $("#existingBanks").html();
        var template = Handlebars.compile(existingBanks);
        var context = { banks: data };
        var html = template(context);

        $("#home").html(html);
    });



    
};

var SettingsBankDetails = function(id) {
    $('#tabBanks li:eq(1) a').tab('show');
};