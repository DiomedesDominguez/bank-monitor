﻿/// <reference path="_references.js" />

var AppViewModel;

$(document).ready(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });

    AppViewModel = {
        Banks: '/api/Banks/'
    };
});
