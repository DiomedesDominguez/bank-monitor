﻿using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;
using Elmah;

namespace MegsoftEvaluation.Web
{
    public class UnhandledExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            ErrorLog.GetDefault(HttpContext.Current).Log(new Error(context.Exception));
        }
    }
}
