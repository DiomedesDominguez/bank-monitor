﻿using System;
using System.Web.Mvc;
using PostSharp.Aspects;

namespace MegsoftEvaluation.Web
{
    [Serializable]
    [AttributeUsage(AttributeTargets.Method)]
    public class JsonResultExceptionAttribute : OnExceptionAspect
    {
        private readonly JsonRequestBehavior _jsonRequest;

        public JsonResultExceptionAttribute(JsonRequestBehavior jsonRequest = JsonRequestBehavior.DenyGet)
        {
            _jsonRequest = jsonRequest;
        }

        public override sealed void OnException(MethodExecutionArgs args)
        {
            base.OnException(args);

            args.ReturnValue = new JsonResult
            {
                Data = new {Result = "ERROR", args.Exception.Message},
                JsonRequestBehavior = _jsonRequest, //JsonRequestBehavior.DenyGet,
                ContentEncoding = null,
                ContentType = null
            };

            args.FlowBehavior = FlowBehavior.Return;
        }
    }
}