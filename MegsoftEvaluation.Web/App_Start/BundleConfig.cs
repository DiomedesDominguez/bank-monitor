﻿using System.Web;
using System.Web.Optimization;

namespace MegsoftEvaluation.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*",
                "~/Scripts/jquery.unobtrusive-ajax*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/icheck").Include(
                "~/Scripts/icheck.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/morris.js",
                "~/Scripts/jquery.sparkline.js",
                "~/Scripts/jquery-jvectormap-{version}.js",
                "~/Scripts/jquery-jvectormap-world-mill-en.js",
                "~/Scripts/jquery.knob.js",
                "~/Scripts/daterangepicker.js",
                "~/Scripts/bootstrap-datepicker.js",
                "~/Scripts/bootstrap3-wysihtml5.all.js",
                "~/Scripts/icheck.js",
                "~/Scripts/jquery.slimscroll.js",
                "~/Scripts/fastclick.js",
                "~/Scripts/app.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                "~/Scripts/knockout-{version}.js",
               "~/Scripts/handlebars.js"));

            bundles.Add(new ScriptBundle("~/bundles/views").IncludeDirectory(
                "~/Scripts/views/", "*.js"));

            bundles.Add(new ScriptBundle("~/bundles/default").Include(
                "~/Scripts/Default.js"));

            bundles.Add(new StyleBundle("~/bundles/emptyCss").Include(
                "~/Content/bootstrap.min.css",
                "~/Content/font-awesome.min.css",
                "~/Content/AdminLTE.min.css",
                "~/Content/blue.min.css"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                "~/Content/bootstrap.min.css",
                "~/Content/AdminLTE.min.css",
                "~/Content/skin-blue.min.css",
                "~/Content/blue.min.css",
                "~/Content/morris.min.css",
                "~/Content/jquery-jvectormap-1.2.2.min.css",
                "~/Content/datepicker3.min.css",
                "~/Content/daterangepicker-bs3.min.css",
                "~/Content/bootstrap3-wysihtml5.min.css"));
#if (!DEBUG)
{  
            BundleTable.EnableOptimizations = true;
}
#endif
        }
    }
}
