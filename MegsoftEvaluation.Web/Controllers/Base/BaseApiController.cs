﻿using System.Web.Http;
using MegsoftEvaluation.Web.Models;

namespace MegsoftEvaluation.Web.Controllers
{
    public class BaseApiController : ApiController
    {
        public ApplicationDbContext db { get; private set; }

        public BaseApiController()
        {
            db = new ApplicationDbContext();
        }
    }
}
