﻿using System.Web.Mvc;
using MegsoftEvaluation.Web.Models;

namespace MegsoftEvaluation.Web.Controllers
{
    public class BaseController : Controller
    {
        public ApplicationDbContext db { get; private set; }

        public BaseController()
        {
            db = new ApplicationDbContext();
        }
    }
}