﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MegsoftEvaluation.DataAccess;

namespace MegsoftEvaluation.Web.Controllers
{
    [Authorize]
    public class BanksController : BaseApiController
    {
        // GET: api/Banks
        public IQueryable<Banks> GetBanks()
        {
            return db.Banks;
        }

        // GET: api/Banks/5
        [ResponseType(typeof(Banks))]
        public IHttpActionResult GetBanks(int id)
        {
            Banks banks = db.Banks.Find(id);
            if (banks == null)
            {
                return NotFound();
            }

            return Ok(banks);
        }

        // PUT: api/Banks/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutBanks(int id, Banks banks)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != banks.Id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(banks).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!BanksExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        // POST: api/Banks
        [ResponseType(typeof(Banks))]
        public IHttpActionResult PostBanks(Banks banks)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Banks.AddOrUpdate(banks);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = banks.Id }, banks);
        }

        // DELETE: api/Banks/5
        [ResponseType(typeof(Banks))]
        public IHttpActionResult DeleteBanks(int id)
        {
            Banks banks = db.Banks.Find(id);
            if (banks == null)
            {
                return NotFound();
            }
            banks.RegisterStatus = RegisterStatus.Deleted;
            db.Banks.AddOrUpdate(banks);
            db.SaveChanges();

            return Ok(banks);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //private bool BanksExists(int id)
        //{
        //    return db.Banks.Count(e => e.Id == id) > 0;
        //}
    }
}