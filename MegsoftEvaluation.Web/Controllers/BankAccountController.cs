﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MegsoftEvaluation.Web.Controllers
{
    [Authorize]
    public class BankAccountController : Controller
    {
        // GET: BankAccount
        public ActionResult Index()
        {
            return Dashboard();
        }

        public ActionResult Dashboard()
        {
            return View();
        }
        public ActionResult Manage()
        {
            return View();
        }

        public ActionResult Accounts()
        {
            return View();
        }

        public ActionResult Transactions()
        {
            return View();
        }

        public ActionResult Settings()
        {
            return View();
        }
    }
}