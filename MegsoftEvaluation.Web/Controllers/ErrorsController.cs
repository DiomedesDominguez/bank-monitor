﻿using System.Web.Mvc;
using MegsoftEvaluation.Web.Models;
using System;

namespace MegsoftEvaluation.Web.Controllers
{
    public class ErrorsController : Controller
    {
        // GET: Errors
        public ActionResult Index()
        {
            ViewBag.Title = "500 Error";
            var model = new ErrorsViewModel
            {
                StatusCode = 500,
                Message = "We will work on fixing that right away.",
                Title = "Oops! Something went wrong."
            };

            return View("Error", model);
        }

        public ActionResult NotFound()
        {
            ViewBag.Title = "404 Page not found";
            var model = new ErrorsViewModel
            {
                StatusCode = 404,
                Message = "We could not find the page you were looking for.",
                Title = "Oops! Page not found."
            };

            return View("Error", model);
        }

        public ActionResult ThrowError()
        {
            throw new NotImplementedException("Pew ^ Pew");
        }
    }
}