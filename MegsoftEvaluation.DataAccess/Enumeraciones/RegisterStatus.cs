﻿using System.ComponentModel;

namespace MegsoftEvaluation.DataAccess
{
    public enum RegisterStatus : byte
    {
        [Description("Active")]
        Active = 0,

        [Description("Inactive")]
        Inactive = 1,

        [Description("Deleted")]
        Deleted = 2
    }
}
