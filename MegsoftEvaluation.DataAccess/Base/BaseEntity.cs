﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MegsoftEvaluation.DataAccess
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            Created = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        public RegisterStatus RegisterStatus { get; set; }
        public DateTime Created { get; set; }

        [MaxLength(50)]
        public string CreatedBy { get; set; }

        [MaxLength(50)]
        public string ModifiedBy { get; set; }

        public DateTime? Modified { get; set; }

        [MaxLength(45)]
        public string ClientIp { get; set; }

    }
}
