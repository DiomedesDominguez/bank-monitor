﻿using System.ComponentModel.DataAnnotations;

namespace MegsoftEvaluation.DataAccess
{
    public interface IBanks
    {
        string Name { get; set; }
        string ImageUrl { get; set; }
        string MainUrl { get; set; }
        string UsernameField { get; set; }
        string PasswordField { get; set; }
        bool HasCaptcha { get; set; }
        string CaptchaField { get; set; }
        string LoginButton { get; set; }
        string AccountAliasField { get; set; }
        string AccountField { get; set; }
        string AccountTypeField { get; set; }
        string CurrencyTypeField { get; set; }
        string AmountField { get; set; }
        bool HasCustomTransactionUrl { get; set; }
        string TransactionUrl { get; set; }
        bool HasDateRange { get; set; }
        string FromDateField { get; set; }
        string ToDateField { get; set; }
        bool UseCssClassOrIdTransactionTable { get; set; }
        string TransactionTableCssClassOrId { get; set; }
        string TransactionRowCssClass { get; set; }
        string TransactionColumnCssClass { get; set; }
        short TransactionDateIndex { get; set; }
        short TransactionCheckNumberIndex { get; set; }
        short TransactionRefNumberIndex { get; set; }
        short TransactionDescriptionIndex { get; set; }
        short TransactionAmountIndex { get; set; }
    }
}