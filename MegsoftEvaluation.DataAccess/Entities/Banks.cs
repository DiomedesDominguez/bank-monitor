﻿using System.ComponentModel.DataAnnotations;

namespace MegsoftEvaluation.DataAccess
{
    public class Banks : BaseEntity, IBanks
    {
        [MaxLength(35)]
        public string Name { get; set; }

        [MaxLength(2048)]
        public string ImageUrl { get; set; }

        [MaxLength(2048)]
        public string MainUrl { get; set; }

        [MaxLength(35)]
        public string UsernameField { get; set; }

        [MaxLength(35)]
        public string PasswordField { get; set; }

        public bool HasCaptcha { get; set; }

        [MaxLength(35)]
        public string CaptchaField { get; set; }

        [MaxLength(35)]
        public string LoginButton { get; set; }

        [MaxLength(35)]
        public string AccountAliasField { get; set; }

        [MaxLength(35)]
        public string AccountField { get; set; }

        [MaxLength(35)]
        public string AccountTypeField { get; set; }

        [MaxLength(35)]
        public string CurrencyTypeField { get; set; }

        [MaxLength(35)]
        public string AmountField { get; set; }

        public bool HasCustomTransactionUrl { get; set; }

        [MaxLength(35)]
        public string TransactionUrl { get; set; }

        public bool HasDateRange { get; set; }

        [MaxLength(35)]
        public string FromDateField { get; set; }

        [MaxLength(35)]
        public string ToDateField { get; set; }

        public bool UseCssClassOrIdTransactionTable { get; set; }

        [MaxLength(35)]
        public string TransactionTableCssClassOrId { get; set; }

        [MaxLength(35)]
        public string TransactionRowCssClass { get; set; }

        [MaxLength(35)]
        public string TransactionColumnCssClass { get; set; }

        public short TransactionDateIndex { get; set; }
        public short TransactionCheckNumberIndex { get; set; }
        public short TransactionRefNumberIndex { get; set; }
        public short TransactionDescriptionIndex { get; set; }
        public short TransactionAmountIndex { get; set; }
    }
}